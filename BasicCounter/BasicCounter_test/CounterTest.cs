using System;
using Xunit;

using BasicCounter_classlib;

namespace BasicCounter_test
{
    public class CounterTest
    {
        [Fact]
        public void TestSetGet()
        {
            Counter compte = new Counter();
            Assert.Equal(0, compte.Count);
            compte.Count = 8;
            Assert.Equal(8, compte.Count);
            Assert.NotEqual(9, compte.Count);
        }
        [Fact]
        public void TestAddOne() {
            Counter compte = new Counter();
            Assert.Equal(0, compte.Count);
            compte.Count = 5;
            compte.AddOne();
            Assert.Equal(6, compte.Count);
        }
        [Fact]
        public void TestRmOne() {
            Counter compte = new Counter();
            Assert.Equal(0, compte.Count);
            compte.Count = 6;
            compte.RmOne();
            Assert.Equal(5, compte.Count);
        }
        [Fact]
        public void TestReset() {
            Counter compte = new Counter();
            Assert.Equal(0, compte.Count);
            compte.Count = 5;
            compte.reset();
            Assert.Equal(0, compte.Count);
        }
    }
}
