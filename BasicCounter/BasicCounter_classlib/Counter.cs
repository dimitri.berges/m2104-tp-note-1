﻿using System;

namespace BasicCounter_classlib
{
    public class Counter
    {
        private int count;

        public Counter() {
            this.count = 0;
        }
        public int Count {
            get
            {
                return this.count;
            }
            set
            {
                if (value < 0) this.count = -value;
                else this.count = value;
            }
        }
        public void AddOne() => this.count++;
        public void RmOne() {
            if (this.count > 0) this.count--;
        }
        public void reset() => this.count = 0;
    }
}
